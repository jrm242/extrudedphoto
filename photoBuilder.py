import matplotlib.pyplot as plt
import numpy as np
import math

from plane import Plane
from cacheHandler import Cache
from utils import mapVal
from utils import getBand
from utils import getConnected
import stlWriter 

def run():    # if you change parameters then you need to delete the cache first
    filepath = "received_386420712634663"         
    extension = ".jpeg"
    bands = 8         
    baseHeight = 1
    bandHeight = 0.5
    imageWidth = 70  # controls height not width
    pixelWidth = 0.5

    cache = Cache(filepath)    

    print("Getting raw photo")
    raw = getPhoto(filepath+extension)
    print("Reducing resolution")
    lowRes = precReduceResolution(raw,imageWidth, pixelWidth)
    print("Converting to greyscale")
    greyscale = colourToGrey(lowRes)
    print("Banding the greyscale image")
    bandedGreyscale = bandPhoto(greyscale,bands)
    bandedGreyscalecp = bandedGreyscale.copy()

    if (cache.status == "no cache"):          
        print("Finding horizontal planes")
        planes = findPlanes(bandedGreyscalecp,bands)
        cache.writeHoriPlane(planes)

    if (cache.status == "horizontal planes"): 
        print("Aquiring horizontal plane list from cache")
        cache.getCache()
        planes = cache.readCache()
    
    print("Reformating planes")
    for plane in planes:
        plane.reformat(bands)

    print("Finding and ordering edges in planes") 
    for i in range(len(planes)):
        planes[i].findEdges(bandedGreyscale)

    print("Finding vertices")
    for plane in planes:
        plane.orderEdgesToGetAdjacentHeights(bandedGreyscale,bands)   
        plane.findVertices()

    print("Triangulating planes")
    for plane in planes:
        plane.triangulate()

    print("Adding vertical planes to triangulation")
    for plane in planes:
        plane.addVerticalPlanes()
    
    print("Adding base")
    planes[0].triangulation.append([[-0.5,-0.5,-1],[-0.5,bandedGreyscale.shape[1]-0.5,-1],[bandedGreyscale.shape[0]-0.5,-0.5,-1]])
    planes[0].triangulation.append([[-0.5,bandedGreyscale.shape[1]-0.5,-1],[bandedGreyscale.shape[0]-0.5,bandedGreyscale.shape[1]-0.5,-1],[bandedGreyscale.shape[0]-0.5,-0.5,-1]])

    print("Adding normals and ordering triangle vertices")
    for plane in planes:
        plane.addNormals(bandedGreyscale, bands)
        plane.orderVertices()
    
    print("Scaling model")
    for plane in planes:
        plane.scale(bandHeight, baseHeight, pixelWidth, invert = False)

    print("Writing STL")
    stlWriter.writeSTL(planes,filepath)

def getPhoto(path):
    return plt.imread(path)

def colourToGrey(colr, bright = True):
    ans = colr.copy()
    if bright:
        brightestVal = 0
        darkestVal = 256
    for i in range(colr.shape[0]):
        #print(i,colr.shape[0])
        for j in range(colr.shape[1]):
            brgt = (0.3*colr[i][j][0] + 0.59*colr[i][j][1] + 0.11*colr[i][j][2])/3
            ans[i][j] = [brgt,brgt,brgt]
            if bright:
                if brgt>brightestVal:
                    brightestVal = brgt
                elif brgt<darkestVal:
                    darkestVal = brgt
    if bright:
        ans = brighten(ans,brightestVal,darkestVal)
    return ans

def brighten(gry,brgt,drk):
    print("Brightening image")
    ans = gry.copy()
    for i in range(gry.shape[0]):
        for j in range(gry.shape[1]):
            val = 255*(gry[i][j][0]-drk)/(brgt-drk)
            ans[i][j] = [val,val,val]
    return ans

def precReduceResolution(imag,imageWidth, pixelWidth):  ## currently just samples from centre and technically offset but ok for now
    imagWidth_pix = int(imageWidth/pixelWidth)
    imagHeight_pix = int(imag.shape[1]*imageWidth/(pixelWidth*imag.shape[0]))
    newImag = np.array([[[0,0,0] for j in range(imagHeight_pix)] for i in range(imagWidth_pix)])

    for i in range(newImag.shape[0]):
        for j in range(newImag.shape[1]):
            val0 = imag[mapVal(i,imagWidth_pix,imag.shape[0])][mapVal(j,imagHeight_pix,imag.shape[1])][0]
            val1 = imag[mapVal(i,imagWidth_pix,imag.shape[0])][mapVal(j,imagHeight_pix,imag.shape[1])][1]
            val2 = imag[mapVal(i,imagWidth_pix,imag.shape[0])][mapVal(j,imagHeight_pix,imag.shape[1])][2]

            newImag[i][j] = [val0,val1,val2]

    return newImag

def bandPhoto(gry,bands):
    bndedgry = gry.copy()
    interval = 256/bands 
    for i in range(gry.shape[0]):
        for j in range(gry.shape[1]):
            val = (getBand(gry[i][j][0],bands))*interval
            bndedgry[i][j] = [val,val,val]
    return bndedgry

def findPlanes(imag, bands):
    #split edge pixels by colours
    #interval = 256/bands 
    colourSplitPixels = [[] for x in range(bands)]
    for i in range(imag.shape[0]):
        for j in range(imag.shape[1]):
            band = getBand(imag[i][j][0],bands)
            colourSplitPixels[band].append((i,j))

    #put all connected pixels in a list and pass to Edge class 
    planePixels = []
    for i in range(len(colourSplitPixels)):
        a = colourSplitPixels[i]
        print("Layer "+str(1+i)+" of "+str(bands))
        b = getConnected(a)
        planePixels.extend(b)
        print(str(len(b))+" planes found in layer")

    planes = []
    for planePix in planePixels:
        if len(planePix)>0:
            planes.append(Plane(planePix,getBand(imag[planePix[0][0]][planePix[0][1]][0],bands)))

    return planes

if __name__ == "__main__":
    run()