from plane import Plane
from utils import getBand

def writeSTL(planes, filename):  # need to fix normals and set vertices to be counterclockwise when viewed from outside
    stl_file = open(filename+".stl","w+")
    stl_file.write("solid "+filename)
    for plane in planes:
        if plane.band <100:
            for triangle in plane.triangulation:
                stl_file.write("\n\t"+"facet normal "+vertexToStr(triangle[3]))
                stl_file.write("\n\t\t"+"outer loop")
                stl_file.write("\n\t\t\t"+"vertex "+vertexToStr(triangle[0]))
                stl_file.write("\n\t\t\t"+"vertex "+vertexToStr(triangle[1]))
                stl_file.write("\n\t\t\t"+"vertex "+vertexToStr(triangle[2]))
                stl_file.write("\n\t\t"+"endloop")
                stl_file.write("\n\t"+"endfacet")

    stl_file.write("\n"+"endsolid "+filename)
    stl_file.close()

def getNormal(triangle):
    if triangle[0][2] == triangle[1][2] and triangle[0][2] == triangle[2][2]:
        return "0e0 0e0 1e0"
    elif triangle[0][1] == triangle[1][1] and triangle[0][1] == triangle[2][1]:
        return "0e0 1e0 0e0"
    elif triangle[0][0] == triangle[1][0] and triangle[0][0] == triangle[2][0]:
        return "1e0 0e0 0e0"
    print("failed to assign normal to triangle:")
    print(triangle)
    input()

def vertexToStr(vertex):
    verstr = numToEStr(vertex[0])+" "+numToEStr(vertex[1])+" "+numToEStr(vertex[2])
    return verstr

def numToEStr(num):
    x = num
    n = 0
    if not(-1<x<1):
        while(not(-10<x<10)):
            x /= 10
            n += 1
    else:
        while(not(-10<x<10)):
            x *= 10
            n -= 1
    
    xstr = str(x)+"e"+str(n)
    return xstr