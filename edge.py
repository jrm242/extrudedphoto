import math

from utils import getBand

class Edge:
    ends = (None,None)
    adjacentHeights = None
    delta = None

    def __init__(self, ends):
        self.ends = ends
        self.getDelta()

    def __repr__(self):
        return str(self.ends)

    def getDelta(self):
        self.delta = (self.ends[1][0]-self.ends[0][0],self.ends[1][1]-self.ends[0][1])

    def getAdjacentHeights(self,imag, bands): #smallest stored first
        chk1 = (self.ends[0][0]+0.5*(self.delta[0]+self.delta[1]),self.ends[0][1]+0.5*(self.delta[1]+self.delta[0]))
        chk2 = (self.ends[0][0]+0.5*(self.delta[0]-self.delta[1]),self.ends[0][1]+0.5*(self.delta[1]-self.delta[0]))
        if chk1[0] < 0 or chk1[1]<0 or chk1[0]>=imag.shape[0] or chk1[1]>=imag.shape[1]:                               # denoting out of picture as band -1
            val1 = -1
        else:
            val1 = bands -1 -getBand(imag[int(chk1[0])][int(chk1[1])][0], bands)

        if chk2[0] < 0 or chk2[1]<0 or chk2[0]>=imag.shape[0] or chk2[1]>=imag.shape[1]:
            val2 = -1
        else:
            val2 = bands -1 -getBand(imag[int(chk2[0])][int(chk2[1])][0], bands)
        
        if val2>val1:
            ans = (val1,val2)
        else:
            ans = (val2,val1)

        self.adjacentHeights = ans

