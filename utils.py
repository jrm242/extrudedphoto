import numpy as np
import math

def getBand(val,bands):
    interval = 256/bands 
    band = math.trunc(val/interval)
    if band == bands:
        band -= 1
    return band

def getConnected(fullPixelSet):
    connectedSets = []    
    while len(fullPixelSet) > 0:
        connected = []
        if len(fullPixelSet)<500:
            connected,fullPixelSet = getConnectedSmallSlave(fullPixelSet[0], fullPixelSet)
        else:
            toTest = [fullPixelSet[0]]
            connected += toTest
            while len(toTest) > 0:
                connected, moreToTest, fullPixelSet = getConnectedSlave(toTest[0], fullPixelSet, connected)
                toTest.remove(toTest[0])
                toTest += moreToTest
        
        #print(connected)
        #print(len(fullPixelSet))
        #input()
        connectedSets.append(connected)

    return connectedSets

def getConnectedSlave(origin, pixelSet, connected):
    toTest = []
    pixelSet.remove(origin)

    if checkAdjPixel(0,1,origin,pixelSet,connected):
        toTest.append((origin[0],origin[1]+1))    
    if checkAdjPixel(-1,0,origin,pixelSet,connected):
        toTest.append((origin[0]-1,origin[1]))  
    if checkAdjPixel(1,0,origin,pixelSet,connected):
        toTest.append((origin[0]+1,origin[1]))   
    if checkAdjPixel(0,-1,origin,pixelSet,connected):
        toTest.append((origin[0],origin[1]-1))

    if connected == [None]:
        connected = toTest
    else:
        connected += toTest

    return connected,toTest, pixelSet

def checkAdjPixel(dx,dy,pixel,pixelSet,connected):
    testPix = (pixel[0]+dx,pixel[1]+dy)
    if testPix not in connected:
        if testPix in pixelSet:
            return True
    return False

def getConnectedSmallSlave(origin, pixelSet, connected = [None]):
    "Takes a list of locations on a grid and returns all the locations connected to the origin in the list (diagonals included)"
    
    if connected == [None]:
        connected = [origin]
    else:
        connected.append(origin)
    if len(pixelSet) == 0:
        print("empty pixel set returning")
        return connected,pixelSet
    newPixelSet = pixelSet
    newPixelSet.remove(origin)

    connected,newPixelSet = checkAdjPixelSmall(0,1,origin,newPixelSet,connected)
    connected,newPixelSet = checkAdjPixelSmall(-1,0,origin,newPixelSet,connected)
    connected,newPixelSet = checkAdjPixelSmall(1,0,origin,newPixelSet,connected)
    connected,newPixelSet = checkAdjPixelSmall(0,-1,origin,newPixelSet,connected)

    
    return connected, newPixelSet

def checkAdjPixelSmall(dx,dy,pixel,pixelSet,connected):
    testPix = (pixel[0]+dx,pixel[1]+dy)
    if testPix in pixelSet:
        connected,pixelSet = getConnectedSmallSlave(testPix, pixelSet, connected)
            
    return connected, pixelSet

def getLCOM(a , b, idealFac):
    cmmnFacs = []
    for n in range(3,int(np.round(max(a,b)/2))):
        if a%n == 0 and b%n == 0 and not(n&2 == 0):
            cmmnFacs.append(n)
            if n > idealFac:
                return n
    if len(cmmnFacs)>0:
        return cmmnFacs[-1]
    else:
        print("Failed to find a LCOM")
        return None

def mapVal(val, original, target):
    return int(np.round(val*target/original))