import numpy as np
from edge import Edge
from utils import getConnected
from utils import getBand

class Plane:
    pixels = []
    mainEdges = []
    mainVertices = []
    holeEdges = []
    holeVertices = []
    band = None
    triangulation = []

    def __init__(self, pixels, band):
        self.pixels = pixels
        self.band = band 

    def __repr__(self):
        return (str(self.band)+"\t"+str(len(self.pixels))+"\t"+str(self.pixels)) # doesn't seem to work

    def reformat(self, bands):
        self.band = bands - self.band -1

    def findEdges(self, imag):        
        for pix in self.pixels:  #stored clockwise
            if pix[0] == 0:
                self.mainEdges.append(Edge(((-0.5,pix[1]+0.5),(-0.5,pix[1]-0.5))))
            elif not(imag[pix[0]][pix[1]][0] == imag[pix[0]-1][pix[1]][0]):
                self.mainEdges.append(Edge(((pix[0]-0.5,pix[1]+0.5),(pix[0]-0.5,pix[1]-0.5))))

            if pix[1] == 0:
                self.mainEdges.append(Edge(((pix[0]-0.5,-0.5),(pix[0]+0.5,-0.5))))
            elif not(imag[pix[0]][pix[1]][0] == imag[pix[0]][pix[1]-1][0]):
                self.mainEdges.append(Edge(((pix[0]-0.5,pix[1]-0.5),(pix[0]+0.5,pix[1]-0.5))))

            if pix[0] == imag.shape[0]-1:
                self.mainEdges.append(Edge(((imag.shape[0]-0.5,pix[1]-0.5),(imag.shape[0]-0.5,pix[1]+0.5))))
            elif not(imag[pix[0]][pix[1]][0] == imag[pix[0]+1][pix[1]][0]):
                self.mainEdges.append(Edge(((pix[0]+0.5,pix[1]-0.5),(pix[0]+0.5,pix[1]+0.5))))

            if pix[1] == imag.shape[1]-1:
                self.mainEdges.append(Edge(((pix[0]+0.5,imag.shape[1]-0.5),(pix[0]-0.5,imag.shape[1]-0.5))))
            elif not(imag[pix[0]][pix[1]][0] == imag[pix[0]][pix[1]+1][0]):
                self.mainEdges.append(Edge(((pix[0]+0.5,pix[1]+0.5),(pix[0]-0.5,pix[1]+0.5))))

        self.orderEdges()

    def orderEdges(self):
        self.edgeStartPoints = []        # account for two edges on a plane (currently index of start of each edge stored in edgeStartPoints)
        self.edgeStartPoints.append(0)
        tmpEdges = self.mainEdges.copy()
        newEdges = []
        edgeNum = 0
        newEdges.append([tmpEdges[0]])
        tmpEdges.remove(tmpEdges[0])
                
        while len(tmpEdges)>0:
            notFound = True
            i = 0
            posBackup = None
            while notFound:
                if newEdges[edgeNum][-1].ends[1] == tmpEdges[i].ends[0]:
                    if newEdges[edgeNum][-1].delta[0] == -tmpEdges[i].delta[1] and newEdges[edgeNum][-1].delta[1] == tmpEdges[i].delta[0]:
                        posBackup = tmpEdges[i]
                    else:
                        newEdges[edgeNum].append(tmpEdges[i])
                        tmpEdges.remove(tmpEdges[i])
                        notFound = False
                i += 1
                if i == len(tmpEdges) and notFound:
                    if posBackup == None:
                        newEdges.append([tmpEdges[0]])
                        edgeNum += 1
                        tmpEdges.remove(tmpEdges[0])                            
                    else:
                        newEdges[edgeNum].append(posBackup)
                        tmpEdges.remove(posBackup)
                    notFound = False

        # find main edge (currently just getting longest edge  not neccesarily accurate)
        curLen = 0
        for i in range(len(newEdges)):
            if len(newEdges[i])>curLen:
                edgeNum = i
                curLen = len(newEdges[i])
        self.mainEdges = newEdges[edgeNum]
        newEdges.remove(newEdges[edgeNum])
        self.holeEdges = newEdges.copy()

    def orderEdgesToGetAdjacentHeights(self,imag,bands):
        for i in range(len(self.mainEdges)):
            self.mainEdges[i].getAdjacentHeights(imag,bands)
        for i in range(len(self.holeEdges)):
            for j in range(len(self.holeEdges[i])):
                self.holeEdges[i][j].getAdjacentHeights(imag,bands)
    
    def findVertices(self):
        tmpVert = []   #why is this needed? without they somehow feedback causing all planes to end up with the same vertices
        prevEdge = self.mainEdges[0]
        if not(self.mainEdges[-1].delta == prevEdge.delta) or not(self.mainEdges[-1].adjacentHeights == prevEdge.adjacentHeights):
            tmpVert.append(prevEdge.ends[0])
        for i in range(0,len(self.mainEdges)):   # changed to include 0 not sure if it was a good idea
            if not(self.mainEdges[i].delta == prevEdge.delta) or not(self.mainEdges[i].adjacentHeights == prevEdge.adjacentHeights):
                tmpVert.append(prevEdge.ends[1])
            prevEdge = self.mainEdges[i]
        
        self.mainVertices = tmpVert

        tmpVert = []
        j = 0
        for hole in self.holeEdges:
            tmpVert.append([])
            prevEdge = hole[0]
            if not(hole[-1].delta == prevEdge.delta) or not(hole[-1].adjacentHeights == prevEdge.adjacentHeights):
                tmpVert[j].append(prevEdge.ends[0])
            for i in range(1,len(hole)):
                if not(hole[i].delta == prevEdge.delta) or not(hole[i].adjacentHeights == prevEdge.adjacentHeights):
                    tmpVert[j].append(prevEdge.ends[1])
                prevEdge = hole[i]
            j += 1

        self.holeVertices = tmpVert

    def triangulate(self):
        vertSlices = self.getSlices(1)
        vTriangles = self.getTriangles(vertSlices,1)
        horiSlices = self.getSlices(0)
        hTriangles = self.getTriangles(horiSlices,0)
        if len(hTriangles)<len(vTriangles):
            self.triangulation = hTriangles
        else:
            self.triangulation = vTriangles
    
    def getTriangles(self,slices,dir):
        triangles = []
        for slice_ in slices:
            if not(type(slice_[0]) == type(slices)):
                mini = slice_[0][dir]
                maxi = slice_[0][dir]
                for pix in slice_:
                    if pix[dir]<mini:
                        mini = pix[dir]
                    if pix[dir]>maxi:
                        maxi = pix[dir]       
                if dir == 0:
                    mini = (mini-0.5,slice_[0][1])
                    maxi = (maxi+0.5,slice_[0][1])
                    triangles.append([(mini[0],mini[1]+0.5,self.band),(mini[0],mini[1]-0.5,self.band),(maxi[0],maxi[1]-0.5,self.band)])
                    triangles.append([(maxi[0],maxi[1]-0.5,self.band),(maxi[0],maxi[1]+0.5,self.band),(mini[0],mini[1]+0.5,self.band)])
                elif dir == 1:
                    mini = (slice_[0][0],mini-0.5)
                    maxi = (slice_[0][0],maxi+0.5)
                    triangles.append([(mini[0]-0.5,mini[1],self.band),(mini[0]+0.5,mini[1],self.band),(maxi[0]-0.5,maxi[1],self.band)])
                    triangles.append([(maxi[0]+0.5,maxi[1],self.band),(maxi[0]-0.5,maxi[1],self.band),(mini[0]+0.5,mini[1],self.band)])   
            else:
                #print("split slice, untested code")
                for subSlice in slice_:
                    mini = subSlice[0][dir]
                    maxi = subSlice[0][dir]
                    for pix in subSlice:
                        if pix[dir]<mini:
                            mini = pix[dir]
                        elif pix[dir]>maxi:
                            maxi = pix[dir]
                
                    if dir == 0:
                        mini = (mini-0.5,subSlice[0][1])
                        maxi = (maxi+0.5,subSlice[0][1])
                        triangles.append([(mini[0],mini[1]+0.5,self.band),(mini[0],mini[1]-0.5,self.band),(maxi[0],maxi[1]-0.5,self.band)])
                        triangles.append([(maxi[0],maxi[1]-0.5,self.band),(maxi[0],maxi[1]+0.5,self.band),(mini[0],mini[1]+0.5,self.band)])
                    elif dir == 1:
                        mini = (subSlice[0][0],mini-0.5)
                        maxi = (subSlice[0][0],maxi+0.5)
                        triangles.append([(mini[0]-0.5,mini[1],self.band),(mini[0]+0.5,mini[1],self.band),(maxi[0]-0.5,maxi[1],self.band)])
                        triangles.append([(maxi[0]-0.5,maxi[1],self.band),(maxi[0]+0.5,maxi[1],self.band),(mini[0]+0.5,mini[1],self.band)])   

        return triangles

    def getSlices(self, dir):
        mini = None
        maxi = None
        for pix in self.pixels:
            if mini == None:
                mini = pix[-1*(dir-1)]
                maxi = pix[-1*(dir-1)]
            else:
                if pix[-1*(dir-1)] < mini:
                    mini = pix[-1*(dir-1)]
                elif pix[-1*(dir-1)] > maxi:
                    maxi = pix[-1*(dir-1)]

        unsplitSlices = [ [] for a in range(mini,maxi+1) ]
        for pix in self.pixels:
            unsplitSlices[pix[-1*(dir-1)]-mini].append(pix)

        splitSlices = [ [] for a in range(mini,maxi+1) ]
        for i in range(len(unsplitSlices)):
            a = getConnected(unsplitSlices[i])
            if(len(a) == 1):
                splitSlices[i] = a[0]
            else:
                splitSlices[i] = a

        return splitSlices

    def addVerticalPlanes(self):
        for edge in self.mainEdges:
            if edge.ends[0] in self.mainVertices and self.band == edge.adjacentHeights[1]:
                i = self.mainVertices.index(edge.ends[0])
                i_ = i+1
                if i_ == len(self.mainVertices):
                    i_ = 0
                tri1 = [[self.mainVertices[i][0],self.mainVertices[i][1],self.band],[self.mainVertices[i_][0],self.mainVertices[i_][1],self.band],[self.mainVertices[i][0],self.mainVertices[i][1],edge.adjacentHeights[0]]]
                tri2 = [[self.mainVertices[i][0],self.mainVertices[i][1],edge.adjacentHeights[0]],[self.mainVertices[i_][0],self.mainVertices[i_][1],edge.adjacentHeights[0]],[self.mainVertices[i_][0],self.mainVertices[i_][1],self.band]]
                self.triangulation.append(tri1)
                self.triangulation.append(tri2)

        for j in range(len(self.holeEdges)):
            for edge in self.holeEdges[j]:
                if edge.ends[0] in self.holeVertices[j] and self.band == edge.adjacentHeights[1]:
                    i = self.holeVertices[j].index(edge.ends[0])
                    i_ = i+1
                    if i_ == len(self.holeVertices[j]):
                        i_ = 0
                    tri1 = [[self.holeVertices[j][i][0],self.holeVertices[j][i][1],self.band],[self.holeVertices[j][i_][0],self.holeVertices[j][i_][1],self.band],[self.holeVertices[j][i][0],self.holeVertices[j][i][1],edge.adjacentHeights[0]]]
                    tri2 = [[self.holeVertices[j][i][0],self.holeVertices[j][i][1],edge.adjacentHeights[0]],[self.holeVertices[j][i_][0],self.holeVertices[j][i_][1],edge.adjacentHeights[0]],[self.holeVertices[j][i_][0],self.holeVertices[j][i_][1],self.band]]
                    self.triangulation.append(tri1)
                    self.triangulation.append(tri2)

    def addNormals(self, imag, bands):
        for triangle in self.triangulation:
            if triangle[0][2] == triangle[1][2] and triangle[0][2] == triangle[2][2]:
                if triangle[0][2]>-1:
                    norm = np.array([0,0,1])
                    triangle.append(norm)
                else:
                    norm = np.array([0,0,-1])
                    triangle.append(norm)
            else:
                vec1 = [triangle[0][0]-triangle[1][0], triangle[0][1]-triangle[1][1], triangle[0][2]-triangle[1][2]]
                vec2 = [triangle[0][0]-triangle[2][0], triangle[0][1]-triangle[2][1], triangle[0][2]-triangle[2][2]]
                norm = np.cross(vec1,vec2)
                normSize = (norm[0]**2 + norm[1]**2 + norm[2]**2)**0.5
                norm = np.array(norm)
                norm /= normSize
                if triangle[0][:-1] == triangle[1][:-1]:
                    if triangle[0][-1]>triangle[1][-1]:
                        centre = [(triangle[1][0]+triangle[2][0])/2, (triangle[1][1]+triangle[2][1])/2, (triangle[1][2]+triangle[2][2])/2]
                    else:
                        centre = [(triangle[0][0]+triangle[1][0])/2, (triangle[0][1]+triangle[1][1])/2, (triangle[0][2]+triangle[1][2])/2]
                elif triangle[0][:-1] == triangle[2][:-1]:
                    if triangle[0][-1]>triangle[2][-1]:
                        centre = [(triangle[1][0]+triangle[2][0])/2, (triangle[1][1]+triangle[2][1])/2, (triangle[1][2]+triangle[2][2])/2]
                    else:
                        centre = [(triangle[0][0]+triangle[1][0])/2, (triangle[0][1]+triangle[1][1])/2, (triangle[0][2]+triangle[1][2])/2]
                elif triangle[2][:-1] == triangle[1][:-1]:
                    if triangle[2][-1]>triangle[1][-1]:
                        centre = [(triangle[1][0]+triangle[0][0])/2, (triangle[1][1]+triangle[0][1])/2, (triangle[1][2]+triangle[0][2])/2]
                    else:
                        centre = [(triangle[0][0]+triangle[2][0])/2, (triangle[0][1]+triangle[2][1])/2, (triangle[0][2]+triangle[2][2])/2]
                centre = np.array(centre)
                posPnorm = centre + norm*0.5
                posNnorm = centre - norm*0.5
                if posPnorm[0] < imag.shape[0] and posPnorm[0] > -1 and posPnorm[1] < imag.shape[1] and posPnorm[1] > -1:
                    bandPnorm = getBand(imag[int(posPnorm[0])][int(posPnorm[1])][0], bands)
                else:
                    bandPnorm = bands+1  #linked to below question
                if posNnorm[0] < imag.shape[0] and posNnorm[0] > -1 and posNnorm[1] < imag.shape[1] and posNnorm[1] > -1:
                    bandNnorm = getBand(imag[int(posNnorm[0])][int(posNnorm[1])][0], bands)
                else:
                    bandNnorm = bands+1  #linked to below question
                if bandPnorm<bandNnorm:  #not entirely sure this is the correct one may be greater than
                    norm *= -1
                triangle.append(norm)
                
    def orderVertices(self):
        newTriangulation = []
        for triangle in self.triangulation:
            newTriangle = []
            norm = np.array(triangle[-1])
            rightAngleIndex = self.getRightAngle(triangle)
            a = rightAngleIndex - 1
            b = rightAngleIndex - 2
            if a<0:
                a += 3
            if b<0:
                b += 3
            origin = np.array(triangle[rightAngleIndex])
            rela = np.array(triangle[a]) - origin
            relb = np.array(triangle[b]) - origin

            crossa = np.cross(norm,rela)            
            if self.isPosMultipleOf(crossa,relb):
                newTriangle = [triangle[rightAngleIndex], triangle[a], triangle[b], triangle[-1]]
            else:
                newTriangle = [triangle[rightAngleIndex], triangle[b], triangle[a], triangle[-1]]
            newTriangulation.append(newTriangle)

        self.triangulation = newTriangulation

    def getRightAngle(self,triangle):
        norm = triangle[-1]
        invNorm = np.array([1,1,1]) - norm**2
        v0 = []
        v1 = []
        v2 = []
        for i in range(len(invNorm)):
            if invNorm[i] == 1:
                v0.append(triangle[0][i])
                v1.append(triangle[1][i])
                v2.append(triangle[2][i])
        
        triangle_2d = [v0,v1,v2]
        for i in range(len(triangle_2d)):
            a = i - 1
            b = i - 2
            if a<0:
                a += 3
            if b<0:
                b += 3
            
            if triangle[i][0] == triangle[a][0] and triangle[i][1] == triangle[b][1]:
                return i
            elif triangle[i][1] == triangle[a][1] and triangle[i][0] == triangle[b][0]:
                return i

    def isPosMultipleOf(self, a, b):
        scf = []
        for i in range(len(a)):
            if b[i] == 0:
                scf.append(0)
            else:
                scf.append(a[i]/b[i])

        num = 0
        for sc in scf:
            if sc < 0:
                return False
            if not(sc == 0):
                if num == 0:
                    num = sc
                elif not(sc == num):
                    return False
        return True
    
    def old_orderVertices(self):# needed as vertices in stl must be anticlockwise looking from positve on normal# untested code
        newTriangulation = []
        for triangle in self.triangulation:
            newTriangle = []
            norm = triangle[-1]
            centre = [triangle[0][0] + triangle[1][0] + triangle[2][0], triangle[0][1] + triangle[1][1] + triangle[2][1], triangle[0][2] + triangle[1][2] + triangle[2][2]]
            centre = np.array(centre)
            centre /= 3
            rel0 = np.array(triangle[0][:3]) - centre
            rel1 = np.array(triangle[1][:3]) - centre
            cross0 = np.cross(norm,rel0)
            cross1 = np.cross(norm,rel1)
            #reduce to two dimensions
            invNorm = np.array([1,1,1]) - norm**2
            newRel0 = []
            newRel1 = []
            newCross0 = []
            newCross1 = []
            for i in range(len(invNorm)):
                if invNorm[i] == 1:
                    newRel0.append(rel0[i])
                    newRel1.append(rel1[i])
                    newCross0.append(cross0[i])
                    newCross1.append(cross1[i])
            rel0 = np.array(newRel0)
            rel1 = np.array(newRel1)
            cross0 = np.array(newCross0)
            cross1 = np.array(newCross1)
            #at intercept
            diffPos = rel1 - rel0
            t = cross1[1]*(diffPos[0]) - cross1[0]*(diffPos[1])
            u = cross0[1]*(diffPos[0]) - cross0[0]*(diffPos[1])

            if (t>=0 and u<0):  #why mathmatically
                newTriangle = triangle
                #print("triangle not reversed")
            else:
                newTriangle = [triangle[0],triangle[2],triangle[1],triangle[3]]
                #print("triangle reversed")
            """print("Normal",norm)
            print("Centre", centre)
            print("Relative pos", rel0,rel1)
            print("Crosses",cross0,cross1)
            print(t,u)
            print(triangle)
            print(newTriangle)"""
            #input()

            newTriangulation.append(newTriangle)
        self.triangulation = newTriangulation        

    def scale(self,bandHeight, baseHeight, pixelWidth, invert = False):
        newTriangulation = []
        for triangle in self.triangulation:
            newTriangle = []
            for vertex in triangle[:-1]:
                if vertex[2] == -1:
                    if not(invert):
                        v2 = -baseHeight
                    else:
                        v2 = baseHeight
                else:
                    if not(invert):
                        v2 = vertex[2]*bandHeight
                    else:
                        v2 = vertex[2]*(-bandHeight)

                v0 = vertex[0] * pixelWidth
                v1 = vertex[1] * pixelWidth
                newTriangle.append((v0,v1,v2))
            newTriangle.append(triangle[-1])
            newTriangulation.append(newTriangle)
            
        
        self.triangulation = newTriangulation