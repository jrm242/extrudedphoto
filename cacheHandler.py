from plane import Plane
from edge import Edge

class Cache():
    rootName = ""
    status = "no cache"
    contents = []    

    def __init__(self, rootName):
        self.rootName = rootName
        self.getCache()

    def getCache(self):
        cache_file = open(self.rootName+".txt","a+")
        cache_file.close()
        cache_file = open(self.rootName+".txt","r")
        self.contents = cache_file.readlines()
        cache_file.close()

        if len(self.contents)==0:
            print("No existing cache")
        else:
            if not(self.contents[0] == self.status):
                print("Cache status changed from "+self.status+" to "+self.contents[0][:-1])
                self.status = self.contents[0][:-1]

    def readCache(self):
        if self.status == "no cache":
            return  None
        elif self.status == "horizontal planes":
            return self.readPlane()

    def textSpliter(self, text):
        secStart = 0
        sections = []
        lokingForStart = True
        activeStarts = 0
        for i in range(len(text)):
            if text[i] == "[":
                activeStarts += 1
                if lokingForStart:
                    lokingForStart = False
                    secStart = i                    
            if (not(lokingForStart)) and text[i] == "]":
                activeStarts -= 1
                if activeStarts == 0:
                    sections.append(text[secStart+1:i])
                    lokingForStart = True

        return sections


    def tupleInterpreter_Int(self,text, tupLen = 2):
        subsecStart = 0
        vals = []
        final = []
        lokingForStart = True
        for i in range(len(text)):
            if lokingForStart and text[i] == "(":
                lokingForStart = False
                vals = []
                subsecStart = i+1
            elif (not(lokingForStart)) and text[i] == ",":
                vals.append(int(text[subsecStart:i]))
                subsecStart = i+2
            elif (not(lokingForStart)) and text[i] == ")":
                vals.append(int(text[subsecStart:i]))
                final.append((vals[0],vals[1]))
                lokingForStart = True
        
        return final
    
    def readPlane(self): 
        planes = []
        for line in self.contents[1:]:
            sections = self.textSpliter(line)            
            band = int(sections[0])
            pixLen = int(sections[1])
            pixels = self.tupleInterpreter_Int(sections[2])
            assert(len(pixels) == pixLen)
            edgeLen = int(sections[3])
            edges = []
            #assert(len(edges) == edgeLen)
            p = Plane(pixels,band)
            p.mainEdges = edges
            planes.append(p)
        return planes

    def writeHoriPlane(self, planeList):
        cache_file = open(self.rootName+".txt","w+")
        cache_file.truncate(0)
        cache_file.write("horizontal planes")
        self.status = "horizontal planes"
        for plane in planeList:
            cache_file.write("\n"+"["+str(plane.band)+"] ["+str(len(plane.pixels))+"] "+str(plane.pixels)+" ["+str(len(plane.mainEdges))+"] "+str(plane.mainEdges)+" ["+str(len(plane.holeEdges))+"] "+str(plane.holeEdges) )
        cache_file.close()